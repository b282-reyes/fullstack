
// document - refers to the whole page
// querySelector - used to select a specific object(HTML element) from the document(webpage)
const txtFirstName = document.querySelector("#txt-first-name")
const spanFullName = document.querySelector("#span-full-name")

// Alternatively, we can use the "getElement.." functions to retrieve elements
// 		const txtFirstName = document.getElementById("#txt-first-name")
// 		const txtFirstName = document.getElementById("txt-full-name")

// document.getElementById
// document.getElementByClassName
// document.getElementByTagName

// event - Whenever a user interacts with a webpage, this action is considered as an event
// addEventListener - us a function that takes 2 arguments
// "keyup" - string identifying an event. keyboard event: check this for more events - https://www.javascripttutorial.net/javascript-dom/javascript-keyboard-events/
// Second argument - will execute one the specified is arguments
txtFirstName.addEventListener("keyup", (event) => {
// innerHTML - property that sets or returns the HTML content
	spanFullName.innerHTML = txtFirstName.value

// target - conatins the element where the event happened
	console.log(event.target)
// .value - gets the value of the input object
	console.log(event.target.value)
})